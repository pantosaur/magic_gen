use proc_macro::TokenStream;
use quote::quote;
use syn;

#[proc_macro_derive(Rarity)]
pub fn rarity_derive(input: TokenStream) -> TokenStream {
    let ast = syn::parse(input).unwrap();
    impl_rarity(&ast)
}

fn impl_rarity(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! {
        impl Rarity for #name {
            fn get_rarity(&self) -> u32 {
                self.rarity
            }
        }
    };
    gen.into()
}
