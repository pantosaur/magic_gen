use serde::{Deserialize};
use std::error::Error;
use std::cmp;
use std::fs;
use std::io::Read;
use rand::prelude::*;
use rarity_derive::Rarity;

#[derive(Deserialize, Debug, Rarity)]
#[serde(deny_unknown_fields)] 
struct Property {
    rarity: u32,
    prefix: String,
    suffix: String,
    description: String,
}

#[derive(Deserialize, Debug, Rarity)]
#[serde(deny_unknown_fields)] 
struct Item {
    name: String,
    rarity: u32,
    description: String,
    tags: Vec<String>,
}

#[derive(Deserialize, Debug, Rarity)]
#[serde(deny_unknown_fields)] 
struct Level {
    name: String,
    rarity: u32,
}

#[derive(Deserialize, Debug, Rarity)]
#[serde(deny_unknown_fields)] 
struct PropCount {
    number: u32,
    rarity: u32
}

trait Rarity {
    fn get_rarity(&self) -> u32;
}

#[derive(Deserialize, Debug)]
struct List<T> {
    entries: Vec<T>
}

#[derive(Debug, PartialEq)]
enum Affix {
    Prefix,
    Suffix,
}

struct AffixedProperty<'a> {
    property: &'a Property,
    affix: Option<Affix>,
}

fn main() -> Result<(), Box<dyn Error>> {
    let (itemfile, levelfile, propcountfile) = 
        (
            "data/items.toml",
            "data/level.toml",
            "data/propcount.toml",
        );
    let mut buf = String::new();
    let mut file;

    buf.clear();
    file = fs::File::open(itemfile).unwrap();
    file.read_to_string(&mut buf).unwrap();
    let val: List<Item> = toml::from_str(&buf).unwrap();
    (!val.entries.is_empty()).then_some(()).unwrap();
    let itype = pick_by_rarity(&val.entries);

    buf.clear();
    file = fs::File::open(levelfile).unwrap();
    file.read_to_string(&mut buf).unwrap();
    let val: List<Level> = toml::from_str(&buf).unwrap();
    (!val.entries.is_empty()).then_some(()).unwrap();
    let level = pick_by_rarity(&val.entries);

    buf.clear();
    file = fs::File::open(propcountfile).unwrap();
    file.read_to_string(&mut buf).unwrap();
    let val: List<PropCount> = toml::from_str(&buf).unwrap();
    (!val.entries.is_empty()).then_some(()).unwrap();
    let propcount = pick_by_rarity(&val.entries);

    let mut propfiles = Vec::new();
    for t in &itype.tags[..] {
        let mut filename = "data/magic_".to_owned();
        filename.push_str(&t);
        filename.push_str("_props_");
        filename.push_str(&level.name);
        filename.push_str(".toml");
        let file = fs::File::open(filename);
        match file {
            Ok(mut f) => {
                propfiles.push(String::new());
                f.read_to_string(propfiles.last_mut().unwrap()).unwrap();
            }
            Err(e) => {
                (e.kind() == std::io::ErrorKind::NotFound).then_some(()).unwrap();
            }
        }
    }
    let propfile = propfiles.join("\n\n");
    let val: List<Property> = toml::from_str(&propfile).unwrap_or(List { entries: Vec::new() });

    let mut props = Vec::new();
    let mut has_prefix = false;
    let mut has_suffix = false;
    for _ in 0..cmp::min(propcount.number, val.entries.len() as u32) {
        let prop = pick_by_rarity(&val.entries);
        let r = random::<u64>() % 2 == 0;
        let affix = if (r && !has_prefix) || (!r && has_suffix) {
           has_prefix = true;
           Affix::Prefix
        }
        else {
            has_suffix = true;
            Affix::Suffix
        };
        props.push(AffixedProperty { property: prop, affix: Some(affix) });
    }
    display_output(itype, props);
    Ok(())
}

fn pick_by_rarity<T>(val: &Vec<T>) -> &T
where T: Rarity
{
    let total = val.iter().fold(0, |acc, v| { acc + v.get_rarity() });
    let r = random::<u64>() % total as u64;
    let mut acc = 0;
    let entry = val.into_iter().find_map(|x| {
        acc += x.get_rarity() as u64;
        if acc > r { Some(x) }
        else { None }
    }).unwrap();
    entry
}

fn display_output(item: &Item, props: Vec<AffixedProperty>) {
    let mut prefixed_prop = None;
    let mut suffixed_prop = None;
    for i in 0..props.len() {
        if props[i].affix == Some(Affix::Prefix) {
            prefixed_prop = Some(&props[i]);
            break;
        }
    }
    for i in 0..props.len() {
        if props[i].affix == Some(Affix::Suffix) {
            suffixed_prop = Some(&props[i]);
            break;
        }
    }
    let mut out;
    out = format!("{}", prefixed_prop.map_or("", |p| { &p.property.prefix }));
    out = format!("{}{}", out, if prefixed_prop.is_some() { " " } else { "" });
    out = format!("{}{}", out, &item.name);
    out = format!("{}{}", out, if suffixed_prop.is_some() { " " } else { "" });
    out = format!("{}{}{}", out, suffixed_prop.map_or("", |p| { &p.property.suffix }), "\n");
    out = format!("{}{}", out, &item.description);
    out = format!("{}{}", out, if !&item.description.is_empty() { "\n" } else { "" });
    out = format!("{}{}", out, prefixed_prop.map_or("", |p| { &p.property.description }));
    out = format!("{}{}", out, if prefixed_prop.is_some() { "\n" } else { "" });
    out = format!("{}{}", out, suffixed_prop.map_or("", |p| { &p.property.description }));
    out = format!("{}{}", out, if suffixed_prop.is_some() { "\n" } else { "" });
    print!("{}", out);
}
